import React from 'react';
import { useSelector } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';

import { FixDNavigator, FixSNavigator } from './MainNavigator';


const AppNavigator = props => {
  const isAuth = useSelector(state => !!state.auth.token);

  console.log(isAuth);
  return (
    <NavigationContainer>
      {isAuth && <FixDNavigator />}
      {!isAuth && <FixSNavigator />}
    </NavigationContainer>
  );
};

export default AppNavigator;
