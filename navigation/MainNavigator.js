import React from "react";
import LoginScreen from "../screens/LoginScreen";
import DashboardScreen from "../screens/DashboardScreen";
import SignUpScreen from "../screens/SignUpScreen";
//import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { DrawerItems } from "react-navigation-drawer";
import { Platform, SafeAreaView, Button, View, StyleSheet } from "react-native";
import { useDispatch } from "react-redux";
import * as authActions from "../store/actions/auth";
import { createStackNavigator } from "@react-navigation/stack";
import {
  createDrawerNavigator,
  DrawerItemList,
} from "@react-navigation/drawer";

import { screenOptions as userProductsScreenOptions } from "../screens/DashboardScreen";
import { screenOptions as SignUpScreenOptions } from "../screens/SignUpScreen";
import { screenOptions as DashboardScreenOptions } from "../screens/DashboardScreen";
import { NavigationContainer } from "@react-navigation/native";

const defaultStackNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === "android" ? Colors.accentColor : "",
  },
  // headerTitleStyle: {
  //   fontFamily: "roboto-bold",
  // },
  // headerBackTitleStyle: {
  //   fontFamily: "roboto-light",
  // },
  headerTintColor: Platform.OS === "android" ? "white" : Colors.accentColor,
  headerTitle: "Sayfa",
};

const FixStackNavigator = createStackNavigator();

export const FixSNavigator = () => {
  return (
    <FixStackNavigator.Navigator>
      <FixStackNavigator.Screen
        name="Giriş Yap"
        component={LoginScreen}
        options={{ headerShown: false }}
      />
      <FixStackNavigator.Screen
        name="SignUp"
        component={SignUpScreen}
        options={SignUpScreenOptions}
      />
      <FixStackNavigator.Screen
        name="Dashboard"
        component={DashboardScreen}
        options={DashboardScreenOptions}
      />
    </FixStackNavigator.Navigator>
  );
};

const FixDrawerNavigator = createDrawerNavigator();

export const FixDNavigator = () => {
  const dispatch = useDispatch();

  return (
    <FixDrawerNavigator.Navigator
      drawerContent={(props) => {
        return (
          <View style={{ flex: 1, paddingTop: 20 }}>
            <SafeAreaView forceInset={{ top: "always", horizontal: "never" }}>
              <DrawerItemList {...props} />
              <Button
                title="Logout"
                color={Colors.primary}
                onPress={() => {
                  dispatch(authActions.logout());
                  // props.navigation.navigate('Auth');
                }}
              />
            </SafeAreaView>
          </View>
        );
      }}
      drawerContentOptions={{
        activeTintColor: Colors.primary,
      }}
    >
      <FixDrawerNavigator.Screen
        name="Randevular"
        component={FixSNavigator}
        options={{
          drawerIcon: (props) => (
            <Ionicons
              name={Platform.OS === "android" ? "md-cart" : "ios-cart"}
              size={23}
              color={props.color}
            />
          ),
        }}
      />
    </FixDrawerNavigator.Navigator>
  );
};

// const HealthNavigator = createStackNavigator(
//   {
//     //   Dashboard: DashboardScreen,
//     SignUp: SignUpScreen,
//     // Login:LoginScreen
//   },
//   {
//     defaultNavigationOptions: defaultStackNavOptions,
//   }
// );

// const MainNavigator = createDrawerNavigator(
//   {
//     HealthTab: {
//       screen: HealthNavigator,
//       navigationOptions: {
//         drawerLabel: "Anasayfa",
//         drawerIcon: (drawerConfig) => (
//           <Ionicons name="ios-home" size={21} color={drawerConfig.tintColor} />
//         ),
//       },
//     },
//   },
//   {
//     contentOptions: {
//       activeTintColor: Colors.accentColor,
//     },
//     contentComponent: (props) => {
//       const dispatch = useDispatch();
//       return (
//         <View style={{ flex: 1, paddingTop: 20 }}>
//           <SafeAreaView forceInset={{ top: "always", horizontal: "never" }}>
//             <DrawerItems {...props} />
//             <Button
//               title="Çıkış Yap"
//               color={Colors.TORCH_RED}
//               onPress={() => {
//                 dispatch(authActions.logout());
//                 props.navigation.navigate("Login");
//               }}
//             />
//           </SafeAreaView>
//         </View>
//       );
//     },
//   }
// );

// const SwitchNav = createSwitchNavigator({
//   Login: LoginScreen,
//   // SignUp:SignUpScreen,
//   Tabs: MainNavigator,
// });

// <NavigationContainer>
//       {isAuth && <FixDNavigator />}
//       {!isAuth && <FixSNavigator />}
//     </NavigationContainer>

// export default createAppContainer(SwitchNav);
