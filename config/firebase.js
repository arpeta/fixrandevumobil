import * as firebase from "firebase";
import firestore from "firebase/firestore";

var config = {
  apiKey: "AIzaSyCwPrhj0lMJP5nDhru8xSeoTEeacioPCDU",
  authDomain: "reservation-app-eab2e.firebaseapp.com",
  databaseURL: "https://reservation-app-eab2e.firebaseio.com",
  projectId: "reservation-app-eab2e",
  storageBucket: "reservation-app-eab2e.appspot.com",
  messagingSenderId: "401772968293",
  appId: "1:401772968293:web:c77456d81bc14a0492898c"
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}
firebase.firestore();

export default firebase;
