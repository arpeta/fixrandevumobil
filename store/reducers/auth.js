import { SIGNUP, LOGIN, LOGOUT } from "../actions/auth";

const initialState = {
  userId: null,
  token: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SIGNUP:
      return {
        userId: action.userId,
      };
    case LOGIN:
      return {
        userId: action.userId,
        token: action.token,
      };
    case LOGOUT:
      return initialState;
    default:
      return state;
  }
};
