import React, { useState } from "react";
import ReduxThunk from "redux-thunk";
import AppLoading from "expo-app-loading";
import { combineReducers, createStore, applyMiddleware } from "redux";
import authReducer from "./store/reducers/auth";
import FlashMessage from "react-native-flash-message";
import { Provider } from "react-redux";
import AppNavigator from "./navigation/AppNavigator";
import MainNavigator from "./navigation/MainNavigator";

import * as Font from "expo-font";

const rootReducer = combineReducers({
  auth: authReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
  return (
    <Provider store={store}>
      <AppNavigator />
      <FlashMessage position="top" />
    </Provider>
  );
}
