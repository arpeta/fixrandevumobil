import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";

const DashboardScreen = (props) => {
  return (
    <View><Text>hop</Text></View>
  );
};

export const screenOptions = navData => {
  return {
    headerTitle: "Anasayfa",
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};

export const styles = StyleSheet.create({});

export default DashboardScreen;
