import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  TextInput,
  KeyboardAvoidingView,
  Image,
  ActivityIndicator,
  Alert,
  StatusBar,
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Colors from "../constants/Colors";
import firebase from "../config/firebase";

const LoginScreen = (props) => {
  const [passText, setPass] = useState();
  const [emailText, setEmail] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState({
    secureText: true,
  });
  const [confirmation, setConfirmation] = useState(false);

  const handleMailChange = (text) => {
    setEmail(text);
    if (text.toString().indexOf("@") > -1) {
      setConfirmation(true);
    } else {
      setConfirmation(false);
    }
  };

  const handlePasswordChange = (text) => {
    setPass(text);
  };

  const authHandler = async () => {
    setIsLoading(true);
    try {
      const resData = await firebase
        .auth()
        .signInWithEmailAndPassword(emailText, passText);
      props.navigation.navigate("Dashboard");
    } catch (error) {
      Alert.alert("Hata", "Hata oluştu çıkıp tekrar deneyin!!!", [
        { text: "Tamam" },
      ]);
      setIsLoading(false);
      return;
    }
    setIsLoading(false);
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureText: !data.secureText,
    });
  };

  return (
    <KeyboardAvoidingView>
      <StatusBar barStyle="dark-content"></StatusBar>
      <LinearGradient colors={["#FFFFFF", "#FF5A00"]} style={styles.gradient}>
        <Animatable.View animation="fadeInDownBig" style={styles.container}>
          <Image source={require("../assets/logo.png")} style={styles.logo} />
          <View style={styles.form}>
            <Text style={styles.text_footer}>E-Posta</Text>
            <View style={styles.action}>
              <FontAwesome name="user-o" color="#05375a" size={20} />
              <TextInput
                placeholder="Email Adresiniz"
                style={styles.textInput}
                autoCapitalize="none"
                onChangeText={(text) => handleMailChange(text)}
              />
              {confirmation ? (
                <Animatable.View animation="bounceIn">
                  <Feather name="check-circle" color="green" size={20} />
                </Animatable.View>
              ) : null}
            </View>
            <Text style={[styles.text_footer, { marginTop: 35 }]}>Şifre</Text>
            <View style={styles.action}>
              <FontAwesome name="lock" color="#05375a" size={20} />
              <TextInput
                placeholder="Şifreniz"
                style={styles.textInput}
                autoCapitalize="none"
                secureTextEntry={data.secureText ? true : false}
                onChangeText={(text) => handlePasswordChange(text)}
              />
              <TouchableOpacity onPress={updateSecureTextEntry}>
                {data.secureText ? (
                  <Feather name="eye-off" color="grey" size={20} />
                ) : (
                  <Feather name="eye" color="grey" size={20} />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.button}>
              {isLoading ? (
                <ActivityIndicator
                  style={{ marginTop: 10 }}
                  size="large"
                  color="#FF5A00"
                />
              ) : (
                <TouchableOpacity style={styles.signIn} onPress={authHandler}>
                  <LinearGradient
                    colors={["#ffa97a", "#FF5A00"]}
                    style={[
                      styles.signIn,
                      {
                        marginTop: 50,
                      },
                    ]}
                  >
                    <Text style={[styles.textSign, { color: "#fff" }]}>
                      Giriş Yap
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              )}
            </View>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("SignUp")}
              style={[
                styles.signIn,
                {
                  borderColor: "#ffa97a",
                  borderWidth: 1,
                  marginTop: 40,
                },
              ]}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "#ffa97a",
                  },
                ]}
              >
                Kaydol
              </Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </LinearGradient>
    </KeyboardAvoidingView>
  );
};

export const screenOptions = navData => {
  return {
    headerTitle: "Giriş Yap",
    headerStyle: {
      backgroundColor: "#ff9c71",
    },
    headerTintColor: "#fff",
    
  };
};

export const styles = StyleSheet.create({
  container: {
    marginRight: 30,
    marginLeft: 30,
    marginTop: 60,
    marginBottom: 60,
    flex: 1,
    backgroundColor: Colors.WHITE,
    borderRadius: 30,
    elevation: 5,
    shadowColor: "black",
    shadowOffset: { width: 30, height: 30 },
    shadowRadius: 20,
    shadowOpacity: 0.3,
    padding: 20,
  },
  gradient: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  form: {
    flex: 2,
    justifyContent: "center",
    width: "100%",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
    marginTop: 20,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
  logo: {
    flex: 1,
    width: "80%",
    resizeMode: "contain",
    alignSelf: "center",
  },
});

export default LoginScreen;
